SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';


CREATE SCHEMA IF NOT EXISTS `Sample`
  DEFAULT CHARACTER SET utf8mb4
  COLLATE utf8mb4_0900_ai_ci;
USE `Sample`;

CREATE TABLE IF NOT EXISTS `Sample`.`flower_shop` (
  `id_fshop`             INT         NOT NULL AUTO_INCREMENT,
  `name`                 VARCHAR(45) NOT NULL,
  `flower_quantity_shop` VARCHAR(45) NULL,
  PRIMARY KEY (`id_fshop`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `Sample`.`owner` (
  `id_owner` VARCHAR(45) NOT NULL,
  `name`     VARCHAR(45) NOT NULL,
  `surname`  VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_owner`)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `Sample`.`flower_shop_has_owner` (
  `flower_shop_id_fshop` INT(11)     NOT NULL,
  `owner_id_owner`       VARCHAR(45) NOT NULL,
  PRIMARY KEY (`flower_shop_id_fshop`, `owner_id_owner`),
  INDEX `fk_flower_shop_has_owner_owner1_idx` (`owner_id_owner` ASC) VISIBLE,
  INDEX `fk_flower_shop_has_owner_flower_shop1_idx` (`flower_shop_id_fshop` ASC) VISIBLE,
  CONSTRAINT `fk_flower_shop_has_owner_flower_shop1`
  FOREIGN KEY (`flower_shop_id_fshop`)
  REFERENCES `Sample`.`flower_shop` (`id_fshop`),
  CONSTRAINT `fk_flower_shop_has_owner_owner1`
  FOREIGN KEY (`owner_id_owner`)
  REFERENCES `Sample`.`owner` (`id_owner`)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;


CREATE TABLE IF NOT EXISTS `Sample`.`supplier` (
  `id_supplierr`             INT         NOT NULL,
  `name`                     VARCHAR(45) NULL,
  `surname`                  VARCHAR(45) NULL,
  `flower_quantity_supplier` INT         NULL,
  PRIMARY KEY (`id_supplierr`)
)
  ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `Sample`.`order` (
  `id_order`       INT         NOT NULL,
  `order_quantity` VARCHAR(45) NULL,
  `supplier_id`    INT         NOT NULL,
  `flower_shop_id` INT         NOT NULL,
  PRIMARY KEY (`id_order`, `supplier_id`, `flower_shop_id`),
  INDEX `fk_order_supplier1_idx` (`supplier_id` ASC) VISIBLE,
  INDEX `fk_order_flower_shop1_idx` (`flower_shop_id` ASC) VISIBLE,
  CONSTRAINT `fk_order_supplier1`
  FOREIGN KEY (`supplier_id`)
  REFERENCES `Sample`.`supplier` (`id_supplierr`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_flower_shop1`
  FOREIGN KEY (`flower_shop_id`)
  REFERENCES `Sample`.`flower_shop` (`id_fshop`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB;


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
