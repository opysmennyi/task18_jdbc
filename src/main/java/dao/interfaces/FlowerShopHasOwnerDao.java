package dao.interfaces;

import model.entitydata.Flower_shop_has_owner;

import java.sql.SQLException;

public interface FlowerShopHasOwnerDao extends GeneralDAO<Flower_shop_has_owner, Integer> {

    int delete(Integer fh_id_fshop) throws SQLException;
}
