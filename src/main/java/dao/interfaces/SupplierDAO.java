package dao.interfaces;

import model.entitydata.Supplier;

import java.sql.SQLException;

public interface SupplierDAO extends GeneralDAO<Supplier, Integer> {
    Supplier findById(Integer id) throws SQLException;

    int update(Supplier supplier) throws SQLException;

    int delete(Integer id_client) throws SQLException;
}
