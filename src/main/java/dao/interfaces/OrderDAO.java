package dao.interfaces;

import model.entitydata.Order;

import java.sql.SQLException;

public interface OrderDAO extends GeneralDAO<Order, Integer> {

    int delete(Integer id_order) throws SQLException;
}


