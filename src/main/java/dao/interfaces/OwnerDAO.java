package dao.interfaces;

import model.entitydata.Owner;

import java.sql.SQLException;
import java.util.List;

public interface OwnerDAO extends GeneralDAO<Owner, Integer> {

    int delete(Integer id_owner) throws SQLException;

    Owner findById(String id) throws SQLException;

    List<Owner> findownerid(Integer owner) throws SQLException;
}
