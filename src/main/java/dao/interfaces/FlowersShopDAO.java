package dao.interfaces;

import model.entitydata.Flowers_shop;

import java.sql.SQLException;

public interface FlowersShopDAO extends GeneralDAO<Flowers_shop, Integer> {
    int delete(Integer fh_id_fshop) throws SQLException;
}
