package dao.implementation;

import constants.Constant;
import dao.interfaces.OrderDAO;
import model.entitydata.Order;
import persistant.ConnectionManager;
import transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDaoImpl implements OrderDAO {
    private static final String FIND_ALL = Constant.ORDERDAOIMPL_FIND_ALL;
    private static final String DELETE = Constant.ORDERDAOIMPL_DELETE;
    private static final String CREATE = Constant.ORDERDAOIMPL_CREATE;
    private static final String UPDATE = Constant.ORDERDAOIMPL_UPDATE;
    private static final String FIND_BY_ID = Constant.ORDERDAOIMPL_FIND_BY_ID;

    @Override
    public List<Order> findAll() throws SQLException {
        List<Order> orders = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    orders.add((Order) new Transformer(Order.class).fromResultSetToShop(resultSet));
                }
            }
        }
        return orders;
    }

    @Override
    public Order findById(Integer id_order) throws SQLException {
        Order order = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID)) {
            preparedStatement.setInt(1, id_order);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    order = (Order) new Transformer(Order.class).fromResultSetToShop(resultSet);
                    break;
                }
            }
        }
        return order;
    }

    @Override
    public int create(Order order) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = conn.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, order.getId_order());
            preparedStatement.setString(2, order.getOrder_quantity());
            preparedStatement.setInt(3, order.getSupplier_id());
            preparedStatement.setInt(4, order.getFlower_shop_id());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Order order) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = conn.prepareStatement(UPDATE)) {
            preparedStatement.setInt(1, order.getId_order());
            preparedStatement.setString(2, order.getOrder_quantity());
            preparedStatement.setInt(3, order.getSupplier_id());
            preparedStatement.setInt(4, order.getFlower_shop_id());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Order id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id_order) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = conn.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id_order);
            return preparedStatement.executeUpdate();
        }
    }

}
