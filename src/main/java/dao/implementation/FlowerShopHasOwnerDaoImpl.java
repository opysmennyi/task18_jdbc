package dao.implementation;

import constants.Constant;
import dao.interfaces.FlowerShopHasOwnerDao;
import model.entitydata.Flower_shop_has_owner;
import persistant.ConnectionManager;
import transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FlowerShopHasOwnerDaoImpl implements FlowerShopHasOwnerDao {

    private static final String FIND_ALL = Constant.FSHASOWNERDAOIMPL_FIND_ALL;
    private static final String DELETE = Constant.FSHASOWNERDAOIMPL_DELETE;
    private static final String CREATE = Constant.FSHASOWNERDAOIMPL_CREATE;
    private static final String UPDATE = Constant.FSHASOWNERDAOIMPL_UPDATE;


    @Override
    public List<Flower_shop_has_owner> findAll() throws SQLException {
        List<Flower_shop_has_owner> flower_shop_has_owners = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    flower_shop_has_owners.add((Flower_shop_has_owner) new Transformer(Flower_shop_has_owner.class).fromResultSetToShop(resultSet));
                }
            }
        }
        return flower_shop_has_owners;
    }

    @Override
    public Flower_shop_has_owner findById(Integer integer) throws SQLException {
        return null;
    }


    @Override
    public int create(Flower_shop_has_owner flower_shop_has_owners) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = conn.prepareStatement(CREATE)) {
            preparedStatement.setString(1, flower_shop_has_owners.getO_id_owner());
            preparedStatement.setInt(2, flower_shop_has_owners.getFh_id_fshop());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Flower_shop_has_owner flower_shop_has_owners) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = conn.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, flower_shop_has_owners.getO_id_owner());
            preparedStatement.setInt(2, flower_shop_has_owners.getFh_id_fshop());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Flower_shop_has_owner id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer o_id_owner) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = conn.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, o_id_owner);
            return preparedStatement.executeUpdate();
        }
    }
}