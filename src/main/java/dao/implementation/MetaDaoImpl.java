package dao.implementation;


import model.meta.ColumnMeta;
import model.meta.ForeignKeyMeta;
import model.meta.TableMeta;
import persistant.ConnectionManager;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MetaDaoImpl {
    public List<String> findAllTableName() throws SQLException {
        List<String> tableNames = new ArrayList<>();
        String[] types = {"TABLE"};
        Connection connection = ConnectionManager.getConnection();
        DatabaseMetaData databaseMetaData = connection.getMetaData();
        ResultSet result = databaseMetaData.getTables(connection.getCatalog(), null, "%", types);

        while (result.next()) {
            String tableName = result.getString("TABLE_NAME");
            tableNames.add(tableName);
        }
        return tableNames;
    }

    public List<TableMeta> getTablesStructure() throws SQLException {
        List<TableMeta> tableMetaDataList = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        DatabaseMetaData databaseMetaData = connection.getMetaData();

        String[] types = {"TABLE"};
        String dbName = connection.getCatalog();
        ResultSet result = databaseMetaData.getTables(dbName, null, "%", types);

        while (result.next()) {
            String tableName = result.getString("TABLE_NAME");
            TableMeta tableMetaData = new TableMeta();
            tableMetaData.setDBName(dbName);
            tableMetaData.setTableName(tableName);

            List<String> pkList = new ArrayList<>();
            ResultSet PKs = databaseMetaData.getPrimaryKeys(connection.getCatalog(), null, tableName);
            while (PKs.next()) {
                pkList.add(PKs.getString("COLUMN_NAME"));
            }

            List<ColumnMeta> columnsMetaData = new ArrayList<>();
            ResultSet columnsRS = databaseMetaData.getColumns(dbName, null, tableName, "%");
            while (columnsRS.next()) {
                ColumnMeta columnMetaData = new ColumnMeta();
                columnMetaData.setColumnName(columnsRS.getString("COLUMN_NAME"));
                columnMetaData.setDataType(columnsRS.getString("TYPE_NAME"));
                columnMetaData.setColumnSize(columnsRS.getString("COLUMN_SIZE"));
                boolean cond = columnsRS.getString("IS_NULLABLE").equals("YES") ? true : false;
                columnMetaData.setNullable(cond);
                cond = columnsRS.getString("IS_AUTOINCREMENT").equals("IS_AUTOINCREMENT") ? true : false;
                columnMetaData.setAutoIncrement(cond);

                columnMetaData.setPrimaryKey(false);
                for (String pkName : pkList) {
                    if (columnMetaData.getColumnName().equals(pkName)) {
                        columnMetaData.setPrimaryKey(true);
                        break;
                    }
                }
                columnsMetaData.add(columnMetaData);
            }
            tableMetaData.setColumnMetaData(columnsMetaData);

            List<ForeignKeyMeta> fkMetaDataList = new ArrayList<>();
            ResultSet FKsRS = databaseMetaData.getImportedKeys(dbName, null, tableName);
            while (FKsRS.next()) {
                ForeignKeyMeta foreignKeyMeta = new ForeignKeyMeta();
                foreignKeyMeta.setFkColumnName(FKsRS.getString("FKCOLUMN_NAME"));
                foreignKeyMeta.setPkTableName(FKsRS.getString("PKTABLE_NAME"));
                foreignKeyMeta.setPkColunmName(FKsRS.getString("PKCOLUMN_NAME"));
                fkMetaDataList.add(foreignKeyMeta);
            }
            tableMetaData.setForeignKeyList(fkMetaDataList);

            tableMetaDataList.add(tableMetaData);
        }
        return tableMetaDataList;
    }
}
