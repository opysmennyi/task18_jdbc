package dao.implementation;

import constants.Constant;
import dao.interfaces.FlowersShopDAO;
import model.entitydata.Flowers_shop;
import persistant.ConnectionManager;
import transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FlowersShopDaoImpl implements FlowersShopDAO {
    private static final String FIND_ALL = Constant.FLOWERSSHOPDAOIMPL_FIND_ALL;
    private static final String DELETE = Constant.FLOWERSSHOPDAOIMPL_DELETE;
    private static final String CREATE = Constant.FLOWERSSHOPDAOIMPL_CREATE;
    private static final String UPDATE = Constant.FLOWERSSHOPDAOIMPL_UPDATE;
    private static final String FIND_BY_ID_SHOP = Constant.FLOWERSSHOPDAOIMPL_FIND_BY_ID;


    @Override
    public List<Flowers_shop> findAll() throws SQLException {
        List<Flowers_shop> flowers_shops = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    flowers_shops.add((Flowers_shop) new Transformer(Flowers_shop.class).fromResultSetToShop(resultSet));
                }
            }
        }
        return flowers_shops;
    }

    @Override
    public Flowers_shop findById(Integer id_fshop) throws SQLException {
        Flowers_shop flowers_shop = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_SHOP)) {
            preparedStatement.setInt(1, id_fshop);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    flowers_shop = (Flowers_shop) new Transformer(Flowers_shop.class).fromResultSetToShop(resultSet);
                    break;
                }
            }
        }
        return flowers_shop;
    }

    @Override
    public int create(Flowers_shop flowers_shop) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = conn.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, flowers_shop.getId_fshop());
            preparedStatement.setString(2, flowers_shop.getName_shop());
            preparedStatement.setInt(3, flowers_shop.getFlower_quantity_shop());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Flowers_shop flowers_shop) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = conn.prepareStatement(UPDATE)) {
            preparedStatement.setInt(1, flowers_shop.getId_fshop());
            preparedStatement.setString(2, flowers_shop.getName_shop());
            preparedStatement.setInt(3, flowers_shop.getFlower_quantity_shop());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Flowers_shop id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id_fshop) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = conn.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id_fshop);
            return preparedStatement.executeUpdate();
        }
    }
}