package dao.implementation;

import constants.Constant;
import dao.interfaces.OwnerDAO;
import model.entitydata.Owner;
import persistant.ConnectionManager;
import transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OwnerDaoImpl implements OwnerDAO {
    private static final String FIND_ALL = Constant.OWNERDAOIMPL_FIND_ALL;
    private static final String DELETE = Constant.OWNERDAOIMPL_DELETE;
    private static final String CREATE = Constant.OWNERDAOIMPL_CREATE;
    private static final String UPDATE = Constant.OWNERDAOIMPL_UPDATE;
    private static final String FIND_OWNER_ID = Constant.OWNERDAOIMPL_FIND_BY_OWNER_ID;

    @Override
    public List<Owner> findAll() throws SQLException {
        List<Owner> owners = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    owners.add((Owner) new Transformer(Owner.class).fromResultSetToShop(resultSet));
                }
            }
        }
        return owners;
    }

    @Override
    public Owner findById(Integer integer) throws SQLException {
        return null;
    }

    @Override
    public int create(Owner owner) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setInt(1, owner.getId_owner());
            ps.setString(2, owner.getName_owner());
            ps.setString(3, owner.getSurname_owner());

            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Owner owner) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setInt(1, owner.getId_owner());
            ps.setString(2, owner.getName_owner());
            ps.setString(3, owner.getSurname_owner());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Owner id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id_owner) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setInt(1, id_owner);
            return ps.executeUpdate();
        }
    }

    @Override
    public Owner findById(String id) throws SQLException {
        return null;
    }

    @Override
    public List<Owner> findownerid(Integer owner) throws SQLException {
        List<Owner> owners = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_OWNER_ID)) {
            ps.setInt(1, owner);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    owners.add((Owner) new Transformer(Owner.class).fromResultSetToShop(resultSet));
                }
            }
        }
        return owners;
    }
}
