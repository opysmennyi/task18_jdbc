package dao.implementation;

import constants.Constant;
import dao.interfaces.SupplierDAO;
import model.entitydata.Supplier;
import persistant.ConnectionManager;
import transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SupplierDaoImpl implements SupplierDAO {
    private static final String FIND_ALL = Constant.SUPPLIERDAOIMPL_FIND_ALL;
    private static final String DELETE = Constant.SUPPLIERDAOIMPL_DELETE;
    private static final String CREATE = Constant.SUPPLIERDAOIMPL_CREATE;
    private static final String UPDATE = Constant.SUPPLIERDAOIMPL_UPDATE;
    private static final String FIND_BY_ID_SUPPLIER = Constant.SUPPLIERDAOIMPL_FIND_BY_ID_SUPPLIER;

    @Override
    public List<Supplier> findAll() throws SQLException {
        List<Supplier> suppliers = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    suppliers.add((Supplier) new Transformer(Supplier.class).fromResultSetToShop(resultSet));
                }
            }
        }
        return suppliers;
    }

    @Override
    public Supplier findById(Integer id) throws SQLException {
        Supplier supplier = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_SUPPLIER)) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    supplier = (Supplier) new Transformer(Supplier.class).fromResultSetToShop(resultSet);
                    break;
                }
            }
        }
        return supplier;
    }

    @Override
    public int create(Supplier supplier) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = conn.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, supplier.getId_supplier());
            preparedStatement.setString(2, supplier.getName());
            preparedStatement.setString(3, supplier.getSurname());
            preparedStatement.setInt(4, supplier.getFlower_quantity_sup());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Supplier supplier) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = conn.prepareStatement(UPDATE)) {
            preparedStatement.setInt(1, supplier.getId_supplier());
            preparedStatement.setString(2, supplier.getName());
            preparedStatement.setString(3, supplier.getSurname());
            preparedStatement.setInt(4, supplier.getFlower_quantity_sup());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Supplier id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id_supplier) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = conn.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id_supplier);
            return preparedStatement.executeUpdate();
        }
    }
}

