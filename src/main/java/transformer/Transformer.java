package transformer;

import model.annotation.Column;
import model.annotation.PrimaryKeyComposite;
import model.annotation.Table;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;

public class Transformer<T> {
    private final Class<T> clazz;


    public Transformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Object fromResultSetToShop(ResultSet resultSet) throws SQLException {
        Object shop = null;
        try {
            shop = clazz.getConstructor().newInstance();
            if (clazz.isAnnotationPresent(Table.class)) {
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    if (field.isAnnotationPresent(Column.class)) {
                        Column column = (Column) field.getAnnotation(Column.class);
                        String name = column.name();
                        field.setAccessible(true);
                        Class fieldType = field.getType();
                        if (fieldType == String.class) {
                            field.set(shop, resultSet.getString(name));
                        } else if (fieldType == Integer.class) {
                            field.set(shop, resultSet.getInt(name));
                        }
                    } else if (field.isAnnotationPresent(PrimaryKeyComposite.class)) {
                        field.setAccessible(true);
                        Class fieldType = field.getType();
                        Object FK = fieldType.getConstructor().newInstance();
                        field.set(shop, FK);
                        Field[] fieldsInner = fieldType.getDeclaredFields();
                        for (Field fieldInner : fieldsInner) {
                            if (fieldInner.isAnnotationPresent(Column.class)) {
                                Column column = fieldInner.getAnnotation(Column.class);
                                String name = column.name();
                                fieldInner.setAccessible(true);
                                Class fieldInnerType = fieldInner.getType();
                                if (fieldInnerType == String.class) {
                                    fieldInner.set(FK, resultSet.getString(name));
                                } else if (fieldInnerType == Integer.class) {
                                    fieldInner.set(FK, resultSet.getInt(name));
                                }
                            }
                        }
                    }
                }
            }
        } catch (InstantiationException
                | IllegalAccessException
                | InvocationTargetException
                | NoSuchMethodException e) {
        }

        return shop;
    }
}
