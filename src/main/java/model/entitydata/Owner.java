package model.entitydata;


import model.annotation.PrimaryKey;
import model.annotation.Table;
import model.annotation.Column;

@Table(name = "owner_table")
public class Owner {
    @PrimaryKey
    @Column(name = "id_owner", length = 45)
    private Integer id_owner;
    @Column(name = "name_owner", length = 15)
    private String name_owner;
    @Column(name = "surname_owner", length = 45)
    private String surname_owner;

    public Owner() {
    }

    public Owner(Integer id_owner, String name_owner, String surname_owner) {
        this.id_owner = id_owner;
        this.name_owner = name_owner;
        this.surname_owner = surname_owner;
    }

    public Integer getId_owner() {
        return id_owner;
    }

    public String getName_owner() {
        return name_owner;
    }

    public String getSurname_owner() {
        return surname_owner;
    }

    @Override
    public String toString() {
        return String.format("%-15s %-15s %-15s", id_owner, name_owner, surname_owner);
    }
}

