package model.entitydata;

import model.annotation.Column;
import model.annotation.PrimaryKeyComposite;
import model.annotation.Table;


@Table(name = "flower_shop_has_owner")
public class Flower_shop_has_owner {
    @PrimaryKeyComposite
    @Column(name = "o_id_owner")
    private String o_id_owner;
    @Column(name = "fh_id_fshop")
    private Integer fh_id_fshop;

    public Flower_shop_has_owner() {
    }

    public Flower_shop_has_owner(String o_id_owner, Integer fh_id_fshop) {
        this.o_id_owner = o_id_owner;
        this.fh_id_fshop = fh_id_fshop;
    }

    public String getO_id_owner() {
        return o_id_owner;
    }

    public void setO_id_owner(String o_id_owner) {
        this.o_id_owner = o_id_owner;
    }

    public Integer getFh_id_fshop() {
        return fh_id_fshop;
    }

    public void setFh_id_fshop(Integer fh_id_fshop) {
        this.fh_id_fshop = fh_id_fshop;
    }

    @Override
    public String toString() {
        return String.format("%-11s %d", getO_id_owner(), getFh_id_fshop());
    }
}
