package model.entitydata;

import model.annotation.Column;
import model.annotation.PrimaryKey;
import model.annotation.Table;

@Table(name = "flower_shop")
public class Flowers_shop {
    @PrimaryKey
    @Column(name = "id_fshop")
    private Integer id_fshop;
    @Column(name = "name_shop")
    private String name_shop;
    @Column(name = "flower_quantity_shop")
    private Integer flower_quantity_shop;

    public Flowers_shop() {
    }

    public Flowers_shop(Integer id_fshop, String name_shop, Integer flower_quantity_shop) {
        this.id_fshop = id_fshop;
        this.name_shop = name_shop;
        this.flower_quantity_shop = flower_quantity_shop;
    }

    public Integer getId_fshop() {
        return id_fshop;
    }

    public void setId_fshop(Integer id_fshop) {
        this.id_fshop = id_fshop;
    }

    public String getName_shop() {
        return name_shop;
    }

    public Integer getFlower_quantity_shop() {
        return flower_quantity_shop;
    }

    @Override
    public String toString() {
        return String.format("%-5d %-11s %d", id_fshop, name_shop, flower_quantity_shop);
    }

}
