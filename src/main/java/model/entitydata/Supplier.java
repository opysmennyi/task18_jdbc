package model.entitydata;

import model.annotation.Column;
import model.annotation.PrimaryKey;
import model.annotation.Table;

@Table(name = "supplier")
public class Supplier {
    @PrimaryKey
    @Column(name = "id_supplier")
    private Integer id_supplier;
    @Column(name = "name", length = 45)
    private String name;
    @Column(name = "surname", length = 45)
    private String surname;
    @Column(name = "flower_quantity_sup")
    private Integer flower_quantity_sup;

    public Supplier() {
    }

    public Supplier(Integer id_supplier, String name, String surname, Integer flower_quantity_sup) {
        this.id_supplier = id_supplier;
        this.name = name;
        this.surname = surname;
        this.flower_quantity_sup = flower_quantity_sup;
    }

    public Integer getId_supplier() {
        return id_supplier;
    }

    public void setId_supplier(Integer id_supplier) {
        this.id_supplier = id_supplier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getFlower_quantity_sup() {
        return flower_quantity_sup;
    }

    @Override
    public String toString() {
        return String.format("%d %-15s %-15s %d", id_supplier, name, surname, flower_quantity_sup);
    }
}
