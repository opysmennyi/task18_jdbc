package model.entitydata;

import model.annotation.Column;
import model.annotation.PrimaryKey;
import model.annotation.Table;

@Table(name = "order_table")
public class Order {
    @PrimaryKey
    @Column(name = "id_order")
    private Integer id_order;
    @Column(name = "order_quantity", length = 45)
    private String order_quantity;
    @PrimaryKey
    @Column(name = "supplier_id")
    private Integer supplier_id;
    @PrimaryKey
    @Column(name = "flower_shop_id")
    private Integer flower_shop_id;

    public Order() {
    }

    public Order(Integer id_order, String order_quantity, Integer supplier_id, Integer flower_shop_id) {
        this.id_order = id_order;
        this.order_quantity = order_quantity;
        this.supplier_id = supplier_id;
        this.flower_shop_id = flower_shop_id;
    }

    public Integer getId_order() {
        return id_order;
    }

    public String getOrder_quantity() {
        return order_quantity;
    }

    public Integer getSupplier_id() {
        return supplier_id;
    }

    public Integer getFlower_shop_id() {
        return flower_shop_id;
    }

    @Override
    public String toString() {
        return String.format("%d %-15s %d %d", id_order, order_quantity, supplier_id, flower_shop_id);
    }
}
