package model.meta;

public class ForeignKeyMeta {
    private String pkColunmName;
    private String pkTableName;
    private String fkColumnName;


    public void setPkColunmName(String pkColunmName) {
        this.pkColunmName = pkColunmName;
    }

    public void setPkTableName(String pkTableName) {
        this.pkTableName = pkTableName;
    }

    public void setFkColumnName(String fkColumnName) {
        this.fkColumnName = fkColumnName;
    }

    @Override
    public String toString() {
        return "FK_COLUMN: " + fkColumnName + ",  PK_TABLE: " + pkTableName + ",  PK_COLUMN_NAME: " + pkColunmName;
    }
}
