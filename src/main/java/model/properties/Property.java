package model.properties;

import constants.Constant;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import persistant.ConnectionManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

public class Property {
    public static Logger LOG = LogManager.getLogger(ConnectionManager.class);

    public static String getProperty(String keyToFile) {
        Properties properties = new Properties();
        try (InputStream input = Property.class.getClassLoader().getResourceAsStream(Constant.DBPROPERTIESNAME)) {
            properties.load(Objects.requireNonNull(input));
            return properties.getProperty(keyToFile);
        } catch (IOException e) {
            LOG.error(e);
            e.printStackTrace();
            return null;
        }
    }
}
