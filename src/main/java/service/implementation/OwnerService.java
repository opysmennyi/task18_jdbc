package service.implementation;

import dao.implementation.OwnerDaoImpl;
import model.entitydata.Owner;
import service.inrterfaces.OwnerServices;

import java.sql.SQLException;
import java.util.List;

public class OwnerService implements OwnerServices {
    @Override
    public List<Owner> findAll() throws SQLException {
        return new OwnerDaoImpl().findAll();
    }

    @Override
    public Owner findById(Integer id) throws SQLException {
        return new OwnerDaoImpl().findById(id);
    }

    @Override
    public int create(Owner owner) throws SQLException {
        return new OwnerDaoImpl().create(owner);
    }

    @Override
    public int update(Owner owner) throws SQLException {
        return new OwnerDaoImpl().update(owner);
    }

    @Override
    public int delete(Integer id_owner) throws SQLException {
        return new OwnerDaoImpl().delete(id_owner);
    }
}
