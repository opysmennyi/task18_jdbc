package service.implementation;

import dao.implementation.OrderDaoImpl;
import model.entitydata.Order;
import service.inrterfaces.OrderServices;

import java.sql.SQLException;
import java.util.List;

public class OrderService implements OrderServices {
    @Override
    public List<Order> findAll() throws SQLException {
        return new OrderDaoImpl().findAll();
    }

    @Override
    public Order findById(Integer id) throws SQLException {
        return new OrderDaoImpl().findById(id);
    }

    @Override
    public int create(Order order) throws SQLException {
        return new OrderDaoImpl().create(order);
    }

    @Override
    public int update(Order order) throws SQLException {
        return new OrderDaoImpl().update(order);
    }

    @Override
    public int delete(Integer id_order) throws SQLException {
        return new OrderDaoImpl().delete(id_order);
    }
}
