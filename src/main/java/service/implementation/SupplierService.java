package service.implementation;

import dao.implementation.FlowersShopDaoImpl;
import dao.implementation.OrderDaoImpl;
import dao.implementation.SupplierDaoImpl;
import model.entitydata.Flowers_shop;
import model.entitydata.Supplier;
import persistant.ConnectionManager;
import service.inrterfaces.SupplierServices;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class SupplierService implements SupplierServices {
    @Override
    public List<Supplier> findAll() throws SQLException {
        return new SupplierDaoImpl().findAll();
    }

    @Override
    public Supplier findById(Integer id_order) throws SQLException {
        return new SupplierDaoImpl().findById(id_order);
    }

    @Override
    public int create(Supplier supplier) throws SQLException {
        return new SupplierDaoImpl().create(supplier);
    }

    @Override
    public int update(Supplier supplier) throws SQLException {
        return new SupplierDaoImpl().update(supplier);
    }

    @Override
    public int delete(int id_supplier) throws SQLException {
        return new SupplierDaoImpl().delete(id_supplier);
    }

    @Override
    public int deleteWithMoveSupplier(Integer idDeleted, Integer idMoveTo) throws SQLException {
        int deletedAmount = 0;
        Connection connection = ConnectionManager.getConnection();
        try {
            connection.setAutoCommit(false);
            if (new OrderDaoImpl().findById(idMoveTo) == null) {
                throw new SQLException();
            }

            List<Flowers_shop> shop = (List<Flowers_shop>) new FlowersShopDaoImpl() {
            }.findById(idDeleted);
            for (Flowers_shop flowers_shop : shop) {
                flowers_shop.setId_fshop(idMoveTo);
                new FlowersShopDaoImpl() {

                }.update(flowers_shop);
            }
            deletedAmount = new SupplierDaoImpl().delete(idDeleted);
            connection.commit();
        } catch (SQLException e) {
            if (connection != null) {
                System.err.print("Transaction is being rolled back");
                connection.rollback();
            }
        } finally {
            connection.setAutoCommit(true);
        }
        return deletedAmount;
    }
}
