package service.implementation;

import dao.implementation.FlowersShopDaoImpl;
import model.entitydata.Flowers_shop;
import service.inrterfaces.FlowerShopServices;

import java.sql.SQLException;
import java.util.List;

public class FlowersShopService implements FlowerShopServices {
    @Override
    public List<Flowers_shop> findAll() throws SQLException {
        return new FlowersShopDaoImpl().findAll();
    }

    @Override
    public Flowers_shop findById(Integer id) throws SQLException {
        return new FlowersShopDaoImpl().findById(id);
    }

    @Override
    public int create(Flowers_shop flowers_shop) throws SQLException {
        return new FlowersShopDaoImpl().create(flowers_shop);
    }

    @Override
    public int update(Flowers_shop flowers_shop) throws SQLException {
        return new FlowersShopDaoImpl().update(flowers_shop);
    }

    @Override
    public int delete(Integer fh_id_fshop) throws SQLException {
        return new FlowersShopDaoImpl().delete(fh_id_fshop);
    }

}