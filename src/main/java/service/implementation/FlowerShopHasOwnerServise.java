package service.implementation;

import dao.implementation.FlowerShopHasOwnerDaoImpl;
import model.entitydata.Flower_shop_has_owner;

import java.sql.SQLException;
import java.util.List;

public class FlowerShopHasOwnerServise implements service.inrterfaces.FlowerShopHasOwnerServise {
    @Override
    public List<Flower_shop_has_owner> findAll() throws SQLException {
        return new FlowerShopHasOwnerDaoImpl().findAll();
    }

    @Override
    public Flower_shop_has_owner findById(Integer id) throws SQLException {
        return new FlowerShopHasOwnerDaoImpl().findById(id);
    }

    @Override
    public int create(Flower_shop_has_owner flower_shop_has_owner) throws SQLException {
        return new FlowerShopHasOwnerDaoImpl().create(flower_shop_has_owner);
    }

    @Override
    public int update(Flower_shop_has_owner flower_shop_has_owner) throws SQLException {
        return new FlowerShopHasOwnerDaoImpl().update(flower_shop_has_owner);
    }

    @Override
    public int delete(Integer id_fshop) throws SQLException {
        return new FlowerShopHasOwnerDaoImpl().delete(id_fshop);
    }
}
