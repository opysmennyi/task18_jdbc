package service.inrterfaces;

import model.entitydata.Owner;

import java.sql.SQLException;
import java.util.List;

public interface OwnerServices extends GeneralServices {
    List<Owner> findAll() throws SQLException;

    Owner findById(Integer id) throws SQLException;

    int create(Owner owner) throws SQLException;

    int update(Owner owner) throws SQLException;

    int delete(Integer id_owner) throws SQLException;
}
