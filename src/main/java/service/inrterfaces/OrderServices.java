package service.inrterfaces;

import model.entitydata.Order;

import java.sql.SQLException;
import java.util.List;

public interface OrderServices extends GeneralServices {
    List<Order> findAll() throws SQLException;

    Order findById(Integer id) throws SQLException;

    int create(Order order) throws SQLException;

    int update(Order order) throws SQLException;

    int delete(Integer id_order) throws SQLException;

}
