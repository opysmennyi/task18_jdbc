package service.inrterfaces;

import model.entitydata.Flowers_shop;

import java.sql.SQLException;
import java.util.List;

public interface FlowerShopServices extends GeneralServices {

    List<Flowers_shop> findAll() throws SQLException;

    Flowers_shop findById(Integer id) throws SQLException;

    int create(Flowers_shop flowers_shop) throws SQLException;

    int update(Flowers_shop flowers_shop) throws SQLException;

    int delete(Integer fh_id_fshop) throws SQLException;
}
