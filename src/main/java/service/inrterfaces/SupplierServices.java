package service.inrterfaces;

import model.entitydata.Supplier;

import java.sql.SQLException;
import java.util.List;

public interface SupplierServices extends GeneralServices {

    List<Supplier> findAll() throws SQLException;

    Supplier findById(Integer id_order) throws SQLException;

    int create(Supplier supplier) throws SQLException;

    int update(Supplier supplier) throws SQLException;

    int delete(int id_supplier) throws SQLException;

    int deleteWithMoveSupplier(Integer idDeleted, Integer idMoveTo) throws SQLException;
}
