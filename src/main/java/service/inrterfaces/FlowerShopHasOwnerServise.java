package service.inrterfaces;

import model.entitydata.Flower_shop_has_owner;

import java.sql.SQLException;
import java.util.List;

public interface FlowerShopHasOwnerServise extends GeneralServices {
    List<Flower_shop_has_owner> findAll() throws SQLException;

    Flower_shop_has_owner findById(Integer id) throws SQLException;

    int create(Flower_shop_has_owner flower_shop_has_owner) throws SQLException;

    int update(Flower_shop_has_owner flower_shop_has_owner) throws SQLException;

    int delete(Integer id_fshop) throws SQLException;
}
