package view_controller.view_submodel;

import model.entitydata.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.implementation.*;

import java.sql.SQLException;
import java.util.List;

public class SelectMethods {
    private static Logger LOG = LogManager.getLogger(SelectMethods.class);

    public static void selectSupplier() throws SQLException {
        LOG.info("\nTable: Supplier");
        SupplierService supplierService = new SupplierService();
        List<Supplier> suppliers = supplierService.findAll();
        for (Supplier supplier : suppliers) {
            LOG.trace(supplier);
        }
    }

    public static void selectFlowerShop() throws SQLException {
        LOG.info("\nTable: Flower shop");
        FlowersShopService flowersShopService = new FlowersShopService();
        List<Flowers_shop> flowers_shops = flowersShopService.findAll();
        for (Flowers_shop flowers_shop : flowers_shops) {
            LOG.trace(flowers_shop);
        }
    }

    public static void selectOrder() throws SQLException {
        LOG.info("\nTable: Order");
        OrderService orderService = new OrderService();
        List<Order> orders = orderService.findAll();
        for (Order order : orders) {
            LOG.trace(order);
        }
    }

    public static void selectOwner() throws SQLException {
        LOG.info("\nTable: Owner");
        OwnerService ownerService = new OwnerService();
        List<Owner> owners = ownerService.findAll();
        for (Owner owner : owners) {
            LOG.trace(owner);
        }
    }

    public static void selectFowerShopHasOwner() throws SQLException {
        LOG.info("\nTable: flower_shop_has_owner");
        FlowerShopHasOwnerServise flowerShopHasOwnerServise = new FlowerShopHasOwnerServise();
        List<Flower_shop_has_owner> flower_shop_has_owners = flowerShopHasOwnerServise.findAll();
        for (Flower_shop_has_owner flower_shop_has_owner : flower_shop_has_owners) {
            LOG.trace(flower_shop_has_owner);
        }
    }

    public static void selectAllTable() throws SQLException {
        selectSupplier();
        selectFlowerShop();
        selectOrder();
        selectOwner();
        selectFowerShopHasOwner();
    }
}
