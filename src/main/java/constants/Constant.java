package constants;


import java.util.Scanner;

//Class of constants for task 18 JDBC

public class Constant {

    // ----SUB-MODEL-METHODS--
    public static final String NAME = "Input name for ";
    public static final String SURNAME = "Input surname for ";
    public static final String CREATED = "There are created rows \n";
    public static final String DELETED = "There are deleted rows \n";
    // ----GENERAL-CONSTANTS--
    public static final String DBPROPERTIESNAME = "db.properties";
    public static final String SUPPLIER = "Supplier";
    public static final String FLOWERSHOP = "Flower Shop";
    public static final String ORDER = "Order";
    public static final String OWNER = "Owner";
    public static final String CREATE = "Create ";
    public static final String UPDATE = "Update ";
    public static final String DELETE = "Delete ";
    public static final String SELECT = "Select ";
    public static final String FINDBYID = "Find by ID ";
    public static final String FLOWERSHOPHASOWNER = "flower_shop_has_owner";
    public static final Scanner INPUT = new Scanner(System.in);
    //-----DEPENDENCIES--
    public static final String URL_KEY = "url";
    public static final String USER_KEY = "user";
    public static final String PASSWORD_KEY = "password";
    //-----SUPPLIER-DAO--
    public static final String SUPPLIERDAOIMPL_FIND_ALL = "SELECT * FROM supplier";
    public static final String SUPPLIERDAOIMPL_DELETE = "DELETE FROM supplier WHERE id_supplier=?";
    public static final String SUPPLIERDAOIMPL_CREATE = "INSERT supplier (id_supplier, name, surname, flower_quantity_sup) VALUES (?, ?, ?, ?)";
    public static final String SUPPLIERDAOIMPL_UPDATE = "UPDATE supplier SET name=?, surname=?, flower_quantity_sup=? WHERE id_supplier=?";
    public static final String SUPPLIERDAOIMPL_FIND_BY_ID_SUPPLIER = "SELECT * FROM supplier WHERE id_supplier=?";
    //-----OWNER-DAO--
    public static final String OWNERDAOIMPL_FIND_ALL = "SELECT * FROM owner_table ";
    public static final String OWNERDAOIMPL_DELETE = "DELETE FROM owner_table WHERE id_owner=?";
    public static final String OWNERDAOIMPL_CREATE = "INSERT owner_table (id_owner, name_owner, surname_owner) VALUES (?, ?, ?)";
    public static final String OWNERDAOIMPL_UPDATE = "UPDATE owner_table SET name_owner=?, surname_owner=? WHERE id_owner=?";
    public static final String OWNERDAOIMPL_FIND_BY_OWNER_ID = "SELECT * FROM owner_table WHERE id_owner=?";
    //-----ORDER-DAO--
    public static final String ORDERDAOIMPL_FIND_ALL = "SELECT * FROM order_table ";
    public static final String ORDERDAOIMPL_DELETE = "DELETE FROM order_table WHERE id_order=?";
    public static final String ORDERDAOIMPL_CREATE = "INSERT order_table (id_order, order_quantity, supplier_id, flower_shop_id) VALUES (?, ?, ?, ?)";
    public static final String ORDERDAOIMPL_UPDATE = "UPDATE order_table SET id_order=?, order_quantity=?, supplier_id=? WHERE flower_shop_id=?";
    public static final String ORDERDAOIMPL_FIND_BY_ID = "SELECT * FROM order_table WHERE id_order=?";
    //-----FLOWERS-SHOP-DAO--
    public static final String FLOWERSSHOPDAOIMPL_FIND_ALL = "SELECT * FROM flower_shop ";
    public static final String FLOWERSSHOPDAOIMPL_DELETE = "DELETE FROM flower_shop WHERE id_fshop=?";
    public static final String FLOWERSSHOPDAOIMPL_CREATE = "INSERT flower_shop (id_fshop, name_shop, flower_quantity_shop) VALUES (?, ?, ?)";
    public static final String FLOWERSSHOPDAOIMPL_UPDATE = "UPDATE flower_shop SET id_fshop=?, name_shop=? WHERE flower_quantity_shop=?";
    public static final String FLOWERSSHOPDAOIMPL_FIND_BY_ID = "SELECT * FROM flower_shop WHERE id_fshop=?";
    //-----FLOWERS-SHOP-HAS-OWNER--
    public static final String FSHASOWNERDAOIMPL_FIND_ALL = "SELECT * FROM flower_shop_has_owner ";
    public static final String FSHASOWNERDAOIMPL_DELETE = "DELETE FROM flower_shop_has_owner WHERE fh_id_fshop=? AND o_id_owner=?";
    public static final String FSHASOWNERDAOIMPL_CREATE = "INSERT flower_shop_has_owner (o_id_owner, fh_id_fshop) VALUES (?, ?)";
    public static final String FSHASOWNERDAOIMPL_UPDATE = "UPDATE flower_shop_has_owner SET fhid_fshopfh_id_fshop=? WHERE o_id_owner=?";
}
